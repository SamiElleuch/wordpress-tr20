<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'change-with-strong-password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'o*X,|IVp>%!!rg{K n_XBles;X xT(/9+^z`Dk6F@5|UzU_!sKI7g):COi%4]_(;' );
define( 'SECURE_AUTH_KEY',  '+(nmI`QAPjB@yx&^1vCi/hff%}]WcJSJYNl}2e5=.ZV}L!,ZhD?i6c8ON98qor6z' );
define( 'LOGGED_IN_KEY',    '[4-qvUk?Afu#T/~U2<^+Nb6S/Vy~p$oUr(n7Jd{k@`V5TBp$iNmLV)t*G3|P%M(6' );
define( 'NONCE_KEY',        'A6UUbDe<L3bM/0XpE?MlD6``~fUs;s1aw<Z!m/Y?Gamsfk^y_V.<e4ZS9ydPg6%7' );
define( 'AUTH_SALT',        ':gJ$q3FubXiP*Ux/t;{w(c.tK`xD^x_=[Of,aTF$;S3tSRjU4jBn=q`#/pq<74i-' );
define( 'SECURE_AUTH_SALT', 'Y*0?`#YYD6Yxn}_k6#ka#]wMF0Nyfn^TP vnktoV.ixE%h!g[i&jyz3j+2V}j k<' );
define( 'LOGGED_IN_SALT',   '5q_)K,`>Z$`C^SyvAu*M@Jar*Q6F%kT6S#OP*[DI>x3T`%|e9fn[Kr&?(57P%^ #' );
define( 'NONCE_SALT',       'WRIUF4ozx;`)%YH6)$x>tsqHxrd<(b&Cw_J<KkYJQoY.u$2Hl2D1/;qJgyhwKfng' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );


