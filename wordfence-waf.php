<?php
// Before removing this file, please verify the PHP ini setting `auto_prepend_file` does not point to this.

// This file was the current value of auto_prepend_file during the Wordfence WAF installation (Wed, 29 Jan 2020 13:27:55 +0000)
if (file_exists('/var/www/html/wordfence-waf.php')) {
	include_once '/var/www/html/wordfence-waf.php';
}
if (file_exists('/var/www/html/wp-content/plugins/wordfence/waf/bootstrap.php')) {
	define("WFWAF_LOG_PATH", '/var/www/html/wp-content/wflogs/');
	include_once '/var/www/html/wp-content/plugins/wordfence/waf/bootstrap.php';
}
?>